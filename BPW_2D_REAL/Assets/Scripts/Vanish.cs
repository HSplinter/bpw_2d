﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Vanish : MonoBehaviour
{
    Material material;

    bool isVanishing = false;
    float fade = 1f;


    void Start()
    {
        material = GetComponent<SpriteRenderer>().material;
        
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            isVanishing = true;

        }
        if (isVanishing)
        {
            fade -= Time.deltaTime;

            if (fade <= 0f)
            {
                fade = 0f;
                isVanishing = false;

            }

            material.SetFloat("_Fade", fade);

        }
        
    }
}
